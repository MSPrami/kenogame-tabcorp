**Instructions for Building and Testing the Application**

- Run git clone "repository url" in the command line.
- Run the command "yarn" to get the node modules.
- Run the command "yarn test" to run all tests in the app.
- Run the command "yarn start" to build and run the app
- Now you can see the app in browser.

---
