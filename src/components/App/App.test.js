import React from 'react';
import Adapter from 'enzyme-adapter-react-16.3'
import Enzyme, { shallow } from 'enzyme';
import App from './App';

Enzyme.configure({ adapter: new Adapter() });

describe('Test App', () => {
   it('App renders without crashing', () => {
      shallow(<App />);
    });
});