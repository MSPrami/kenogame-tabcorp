export const DRAW_BALL = 'DRAW_BALL';
export const GENERATE_TICKET_NUMBER_LIST = 'GENERATE_TICKET_NUMBER_LIST';

export function drawBallAction(drawnBallList) {
  return {
    type: DRAW_BALL,
    drawnBallList,
  };
}

export function generateTicketNumberList(selectedTicketNumberList) {
  return {
    type: GENERATE_TICKET_NUMBER_LIST,
    selectedTicketNumberList,
  };
}