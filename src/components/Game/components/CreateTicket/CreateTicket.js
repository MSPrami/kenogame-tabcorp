import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';

import { generateTicketNumberList } from '../../KenoGameActions';
import { getDrawnBallList } from '../../KenoGameSelectors';
import { sortList } from '../../../../utility/utility';

const selectedNumberList = [];

class CreateTicket extends Component {
  constructor(props) {
    super(props);
    this.state = { selectedNumberList: [] };
  }

  toggleNumber(selectedNumber, type) {
    if (type === 'select') {
      selectedNumberList.push(selectedNumber);
    } else {
      const i = selectedNumberList.indexOf(selectedNumber);
      if (i !== -1) selectedNumberList.splice(i, 1); 
      this.props.generateTicketNumberList(selectedNumberList);
    }
    this.setState({
      selectedNumberList: sortList(selectedNumberList, 'Asc'),
    });
    if (selectedNumberList.length === 10)
    this.props.generateTicketNumberList(selectedNumberList);
  }

  createDisplayBoard() {
    const buttonList = []
    const maxNumberLenth = 80

    for (let i = 1; i <= maxNumberLenth; i += 1) {
      const numberSelected = this.state.selectedNumberList.includes(i);
      buttonList.push(
        <Button
          key={i}
          type="button"
          variant="secondary"
          className={numberSelected ? 'highlightButton' : ''}
          disabled={this.state.selectedNumberList.length === 10 && !numberSelected}
          onClick={() => {this.toggleNumber(i, numberSelected ? 'unSelect' : 'select');}}>{i}
        </Button>
      );
    }
    return buttonList;
  }

  render() {
    return (
      <div>
        {
          this.props.drawnBallList.length === 0 &&
          <div>
            <h4>
              Please select 10 unique numbers to create the ticket
            </h4>
            <div className="numberButtonDisplayBoard">
              { this.createDisplayBoard() }
            </div>
          </div>
        }
        <Row className="sectionSubHeading bigFont">
          <Col xs={2}><p className="heading">Ticket:</p></Col>
          <Col xs={10}>
            {
              this.state.selectedNumberList.map(selectedNumber => (
                <span key={selectedNumber} className={this.props.drawnBallList.includes(selectedNumber) ? 'hightLightText' : ''}> {selectedNumber + ' '} </span>
              ))
            }
          </Col>
        </Row>
      </div>
    );
  }

 
}

CreateTicket.defaultProps = {
  drawnBallList: [],
}

CreateTicket.propTypes = {
  drawnBallList: PropTypes.array,
  generateTicketNumberList: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  drawnBallList: getDrawnBallList(state),
});

const mapDispatchToProps = dispatch => ({
  generateTicketNumberList: (selectedTicketNumberList) => dispatch(generateTicketNumberList(selectedTicketNumberList)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateTicket);