import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { drawBallAction } from '../../KenoGameActions';
import { getSelectedTicketNumberList } from '../../KenoGameSelectors';
import { sortList } from '../../../../utility/utility';

const BallDraw = ( {
  drawBallActionFunc,
  selectedTicketNumberList,
}) => {
  
  const maxNoOfBalls = 80;
  const [drawnBallList, setdrawnBallList] = useState([]);
  const [disableButton, setDisableButton] = useState(true);

  useEffect(() => {
    setDisableButton(selectedTicketNumberList.length < 10);
  }, [selectedTicketNumberList.length]);

  const createDisplayBoard = () => {
    const ballList = []

    for (let i = 1; i <= maxNoOfBalls; i += 1) {
      ballList.push(
      <svg key={i} height="50" width="50">
        <circle cx="25" cy="25" r="20" fill={drawnBallList.includes(i) ? '#f05000' : '#d8e4eb' }/>
        {
          drawnBallList.includes(i) &&
          <text x="50%" y="50%" fill="white" textAnchor="middle" dy=".3em">{i}</text>
        }   
      </svg>)
    }
    return ballList;
  }

  let randomNumberList = [];
  const drawBall = () => {
    setDisableButton(true);
    while(randomNumberList.length < 20)
    {
        const r = Math.floor(Math.random()*80) + 1;
        if(randomNumberList.indexOf(r) === -1)
        randomNumberList.push(r);
        randomNumberList = sortList(randomNumberList, 'Asc');                                                           
    }
    setdrawnBallList(randomNumberList);
    drawBallActionFunc(randomNumberList);
  }

  return (
    <div>
      <Row className="bigFont">
        <Col xs={2}><p className="heading">Ball draw:</p></Col>
        <Col xs={10}>
          {
            drawnBallList.map(drawnBall => (
              <span key={drawnBall} className={selectedTicketNumberList.includes(drawnBall) ? 'hightLightText' : ''}> {drawnBall + ' '} </span>
            ))
          }
        </Col>
      </Row>
      <Button
        disabled={disableButton}
        variant={disableButton ? 'secondary' : 'success'}
        className="createTicket sectionHeading"
        onClick={() => {drawBall();}} >
         Draw now
       </Button>
      <div className="ballDisplayContainer sectionHeading">
        {
          createDisplayBoard()
        }
      </div>
    </div>
  );
}

BallDraw.defaultProps = {
  selectedTicketNumberList: [],
};

BallDraw.propTypes = {
  drawBallActionFunc: PropTypes.func.isRequired,
  selectedTicketNumberList: PropTypes.array,
};

const mapStateToProps = state => ({
  selectedTicketNumberList: getSelectedTicketNumberList(state),
});

const mapDispatchToProps = dispatch => ({
  drawBallActionFunc: (drawnBallList) => dispatch(drawBallAction(drawnBallList)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BallDraw);