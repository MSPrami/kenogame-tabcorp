import { sortList } from './utility'

test('sorting test', () => {
      expect(sortList([6, 9, 3], 'Asc')).toStrictEqual([3, 6, 9]);
      expect(sortList([6, 9, 3], 'Desc')).toStrictEqual([9, 6, 3]);

});